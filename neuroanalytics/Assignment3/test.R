temp = list.files(pattern="*.txt")
myfiles = lapply(temp, read.delim)



pdf(file=‘output1.pdf’, onefile = TRUE)
par(mar=rep(0,4))
layout(matrix(1:100,ncol=2,byrow=TRUE))

mmm = array(0, dim = c(100, 10, 5))
for (i in 1:100){ 
    m <- (matrix(, nrow = 10,  ncol = 5))
    s1 = paste("donor", formatC(i, width=3, flag="0"), sep ="")
    
    #plot(0,type='n',axes=FALSE,ann=FALSE)
    #points( rnorm(100), rnorm(100) )
    #points(rep(j,5),m[j,])

    for (j in 1:10){
        s2 = paste("tp", formatC(j, width=3, flag="0"), sep ="")
        cs = paste(s1, s2, sep="_")
        final = paste(cs, ".txt", sep = "")
        m1 = read.table(final, header = TRUE)
        m[j,1:5] = m1[1:5,1]
        
    }
    mmm[i,,] = m
}


pdf(file="longitudinal.pdf",onefile=TRUE)
for (i in 1:100){
    s1 = paste("donor", formatC(i, width=3, flag="0"), sep ="")
    m = mmm[i,,]
    k=1
    plot(rep(k,5),m[k,],xlim=c(-1,11),ylim=c(-5,50000),main=s1,xlab='timepoint',ylab='phenotype')
    for (k in 2:10){
        points(rep(k,5),m[k,])
    }
}
dev.off()
